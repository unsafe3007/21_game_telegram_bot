import random
import time
import telebot
from telebot import types

bot = telebot.TeleBot('5675741324:AAFXtNpYE6WWrU3ZzFExK-e55DbybMMQnOY')

cards = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] * 4
count = 0
dealer_count = 0
AMOUNT = 21
DEALER_AMOUNT = 16
CHAT_ID = 0

rules = '''ПРАВИЛА ИГРЫ:
Каждый ход игрок может взять карту либо остановиться.
Если игрок берет карту он получает от 1 до 10 очков.
Если сумма очков игрока больше 21 - он проиграл.
Если игрок остановился, ход переходит к дилеру.'''


@bot.message_handler(commands=['start'])
def start(message):
    global CHAT_ID

    menu(message.chat.id)

    CHAT_ID = message.chat.id


@bot.callback_query_handler(func=lambda call: True)
def callback(call):
    if call.message:
        if call.data == 'begin':
            start_game(get_card, get_dealer_card)

        elif call.data == 'rules':
            bot.send_message(CHAT_ID, rules)
            menu(CHAT_ID)

        elif call.data == 'exit':
            bot.send_message(CHAT_ID, 'Bye')

        elif call.data == 'continue':
            start_game(get_card, get_dealer_card)

        elif call.data == 'no':
            after_no_choise()

        elif call.data == 'yes':
            clear_result()
            start_game(get_card, get_dealer_card)

        elif call.data == 'nope':
            clear_result()
            menu(CHAT_ID)


def menu(chat_id):
    markup = types.InlineKeyboardMarkup(row_width=3)

    item = types.InlineKeyboardButton('Begin', callback_data='begin')

    item2 = types.InlineKeyboardButton('Rules', callback_data='rules')

    item3 = types.InlineKeyboardButton('Exit', callback_data='exit')

    markup.add(item, item2, item3)

    return bot.send_message(chat_id, "Let's play '21'", reply_markup=markup)


def get_choise():
    markup = types.InlineKeyboardMarkup(row_width=1)

    item = types.InlineKeyboardButton('Yes', callback_data='continue')

    item2 = types.InlineKeyboardButton('No', callback_data='no')

    markup.add(item, item2)

    return bot.send_message(CHAT_ID, "Take a card?", reply_markup=markup)


def is_continue():
    markup = types.InlineKeyboardMarkup(row_width=2)

    item = types.InlineKeyboardButton('Yes', callback_data='yes')

    item2 = types.InlineKeyboardButton('No', callback_data='nope')

    markup.add(item, item2)

    return bot.send_message(CHAT_ID, "Play again?", reply_markup=markup)


def clear_result():
    global count
    global dealer_count

    count = 0
    dealer_count = 0


def get_card():
    global count

    random.shuffle(cards)

    current = cards.pop()

    count += current

    return bot.send_message(CHAT_ID, f'Card is {str(current)}\nPlayer score: -{str(count)}-\n')


def get_dealer_card():
    global dealer_count

    random.shuffle(cards)

    dealer_current = cards.pop()

    dealer_count += dealer_current

    return bot.send_message(CHAT_ID, f'Dealer\'s card is: {dealer_current}\nDealer score: -{dealer_count}-\n')


def first_game_end_conditions(count, dealer_count):
    if count >= AMOUNT or dealer_count >= AMOUNT:
        if count > AMOUNT and (dealer_count >= AMOUNT or dealer_count <= AMOUNT):
            bot.send_message(CHAT_ID, 'You lose!')

            is_continue()

        elif count <= AMOUNT and dealer_count > AMOUNT:
            bot.send_message(CHAT_ID, 'You win!')

            is_continue()


def second_game_end_conditions(count, dealer_count):
    condition_for_lose = (DEALER_AMOUNT < dealer_count <= AMOUNT and AMOUNT > count < dealer_count) or (count > AMOUNT and dealer_count <= AMOUNT)
    condition_for_win = (dealer_count > AMOUNT and count <= AMOUNT) or (dealer_count < AMOUNT and count == AMOUNT) or (dealer_count < count < AMOUNT)

    if condition_for_lose:
        bot.send_message(CHAT_ID, 'You lose!')

    elif condition_for_win:
        bot.send_message(CHAT_ID, 'You win!')

    elif count == dealer_count or (count > AMOUNT and dealer_count > AMOUNT):
        bot.send_message(CHAT_ID, 'Draw!')


def start_game(get_card, get_dealer_card):

        get_card()

        time.sleep(2)

        if dealer_count < 19:
            get_dealer_card()

        time.sleep(1)

        first_game_end_conditions(count, dealer_count)

        get_choise()


def after_no_choise():
    while dealer_count <= DEALER_AMOUNT:
        time.sleep(1)

        get_dealer_card()

        if second_game_end_conditions(count, dealer_count):
            is_continue()
            break


bot.polling(none_stop=True)
